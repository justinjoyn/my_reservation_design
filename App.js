import React, {Component} from 'react';
import {Image, ImageBackground, ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Collapsible from 'react-native-collapsible';

type Props = {};
export default class App extends Component<Props> {

    constructor(props) {
        super(props);
        this.state = {
            arrivalCollapsed: false,
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <StatusBar barStyle={"light-content"} backgroundColor={'transparent'} translucent={true}/>
                <Image source={require('./assets/images/bg.png')} style={styles.backgroundImage}
                       resizeMode={"cover"}/>
                <ScrollView style={styles.mainScroll}>
                    <View style={styles.emptySpace}/>
                    <View style={styles.card}>
                        <View style={styles.cardBody}>
                            <Text style={styles.cardTitle}>My Reservation</Text>
                            <View style={{flexDirection: 'row'}}>
                                <View style={{flexDirection: 'column', alignItems: 'center'}}>
                                    <Image source={require('./assets/images/flight_dep.png')}
                                           style={{marginBottom: 5}}/>
                                    {!this.state.arrivalCollapsed
                                        ?
                                        <Image source={require('./assets/images/line.png')} style={{marginBottom: 5}}/>
                                        : <View style={{height: 40}}/>
                                    }
                                </View>
                                <View style={{flexDirection: 'column', marginLeft: 10, flex: 1}}>
                                    <Text style={{fontSize: 18,}}>Paris CDG Terminal 2A</Text>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text style={{marginRight: 5}}>Lorem ipsum</Text>
                                        <ImageBackground
                                            source={require('./assets/images/text_highlight.png')}
                                            style={{width: 50}} resizeMode={'contain'}>
                                            <Text>14h05</Text>
                                        </ImageBackground>

                                    </View>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text style={{color: '#AAA'}}>Modifier</Text>
                                        <Icon name="edit" size={12} color="#AAA"
                                              style={{padding: 3, marginLeft: 5}}/>
                                    </View>
                                </View>
                                <View>
                                    <TouchableOpacity
                                        onPress={() => this.setState({arrivalCollapsed: !this.state.arrivalCollapsed})}>
                                        <Icon
                                            name={this.state.arrivalCollapsed ? "keyboard-arrow-right" : "keyboard-arrow-down"}
                                            size={30} color="#AAA"
                                            style={{paddingHorizontal: 20, paddingVertical: 10}}/>
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <Collapsible
                                duration={500}
                                collapsed={this.state.arrivalCollapsed}>
                                <View style={{flexDirection: 'row'}}>
                                    <Image source={require('./assets/images/flight_arr.png')}/>
                                    <View style={{flexDirection: 'column', marginLeft: 10}}>
                                        <Text style={{fontSize: 18,}}>Paris CDG Terminal 2A</Text>
                                        <View style={{flexDirection: 'row'}}>
                                            <Text style={{marginRight: 5}}>Lorem ipsum</Text>
                                            <ImageBackground
                                                source={require('./assets/images/text_highlight.png')}
                                                style={{width: 50}} resizeMode={'contain'}>
                                                <Text>14h05</Text>
                                            </ImageBackground>

                                        </View>
                                        <View style={{flexDirection: 'row'}}>
                                            <Text style={{color: '#AAA'}}>modifier</Text>
                                            <Icon name="edit" size={12} color="#AAA"
                                                  style={{padding: 3, marginLeft: 5}}/>
                                        </View>
                                    </View>
                                </View>
                            </Collapsible>
                            <View style={{
                                flexDirection: 'row',
                                alignItems: 'center',
                                width: '100%',
                                justifyContent: 'center',
                                marginVertical: 15
                            }}>
                                <Text style={{color: '#AAA'}}>Lorem ipsum sit amet</Text>
                                <Icon name="access-time" size={12} color="#AAA" style={{padding: 3, marginLeft: 5}}/>
                            </View>
                            <TouchableOpacity style={styles.buttonLarge}>
                                <Text style={{padding: 10}}>LOCALISER LE LIEU DE RDV</Text>
                            </TouchableOpacity>
                            <View style={{flexDirection: 'row', marginTop: 20, justifyContent: 'space-between'}}>
                                <View style={styles.infoBox}>
                                    <Image source={require('./assets/images/flight_arr.png')}/>
                                    <Text>John Doe</Text>
                                    <Text style={{color: '#AAA'}}>+33 6 25 83 38 91</Text>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text style={{color: '#AAA'}}>modifier</Text>
                                        <Icon name="edit" size={12} color="#AAA"
                                              style={{padding: 3, marginLeft: 5}}/>
                                    </View>
                                </View>
                                <View style={{width: 5}}/>
                                <View style={styles.infoBox}>
                                    <Image source={require('./assets/images/flight_arr.png')}/>
                                    <Text>John Doe</Text>
                                    <Text style={{color: '#AAA'}}>+33 6 25 83 38 91</Text>
                                    <View style={{flexDirection: 'row'}}>
                                        <Text style={{color: '#AAA'}}>modifier</Text>
                                        <Icon name="edit" size={12} color="#AAA"
                                              style={{padding: 3, marginLeft: 5}}/>
                                    </View>
                                </View>
                            </View>
                        </View>
                        <View style={styles.cardFooter}>
                            <TouchableOpacity
                                style={{alignItems: 'center', borderRightColor: '#CCC', borderRightWidth: 1, flex: 1}}>
                                <View style={{flexDirection: 'row', padding: 15}}>
                                    <Text style={{color: '#AAA'}}>Noter le voiturier</Text>
                                    <Icon name="star" size={12} color="#AAA"
                                          style={{padding: 4, marginLeft: 3}}/>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity style={{alignItems: 'center', flex: 1}}>
                                <View style={{flexDirection: 'row', padding: 15}}>
                                    <Text style={{color: '#AAA'}}>Noter le voiturier</Text>
                                    <Icon name="euro-symbol" size={12} color="#AAA"
                                          style={{padding: 4, marginLeft: 3}}/>
                                </View>
                            </TouchableOpacity>

                        </View>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        ...StyleSheet.absoluteFillObject
    },
    backgroundImage: {
        ...StyleSheet.absoluteFillObject,
        width: '100%',
        height: '100%'
    },
    mainScroll: {
        ...StyleSheet.absoluteFillObject,
        padding: 15
    },
    emptySpace: {
        height: 200
    },
    card: {
        backgroundColor: '#FFFFFF',
        borderRadius: 10,
        marginBottom: 50
    },
    cardTitle: {
        alignSelf: 'center',
        color: '#CCC',
        paddingTop: 10,
        paddingBottom: 20
    },
    cardBody: {
        padding: 15,
    },
    cardFooter: {
        flexDirection: 'row',
        marginTop: 15,
        justifyContent: 'space-around',
        borderTopColor: '#CCC',
        borderTopWidth: 1
    },
    buttonLarge: {
        width: '100%',
        backgroundColor: '#FFCC1A',
        alignItems: 'center',
        borderRadius: 5
    },
    infoBox: {
        borderColor: '#CCC',
        borderRadius: 5,
        borderWidth: 1,
        alignItems: 'center',
        padding: 15,
        flex: 1,
    }
});
